
import csv
import random

t1_bat_order = []
t1_bow_order = []
t2_bat_order = []
t2_bow_order = []

discrete_list = [0, 1, 2, 3, 4, 6, 7] #i use 7 to signal wicket
									  



# Extraction of squads from the CSV 
with open('Input/TestInputMatch4.csv', 'rb') as f:
    match_reader = csv.reader(f)
    match_reader.next()
    for row in match_reader:
        t1_bat_order.append(row[0])
        t1_bow_order.append(row[1])
        t2_bat_order.append(row[2])
        t2_bow_order.append(row[3])

t1_bat_order = [x for x in t1_bat_order if x != '']
t1_bow_order = [x for x in t1_bow_order if x != '']
t2_bat_order = [x for x in t2_bat_order if x != '']
t2_bow_order = [x for x in t2_bow_order if x != '']

t1_bow_order = t1_bow_order[:5] # Restricting to 5 bowlers
t2_bow_order = t2_bow_order[:5]

# To identify which cluster 
def cluster_number(batsman, bowler) :

	
	with open('Data/BatsmenCluster.csv', 'rb') as f:
	    bat_cluster_reader = csv.reader(f)
	    for row in bat_cluster_reader:
	    	if batsman == row[0]:
	    		curr_bat_cluster_num = row[14]


	
	with open('Data/BowlersCluster.csv', 'rb') as f:
	    bow_cluster_reader = csv.reader(f)
	    for row in bow_cluster_reader:
	    	if bowler == row[0]:
	    		curr_bow_cluster_num = row[13]

	return curr_bat_cluster_num, curr_bow_cluster_num



def pvp_plist(batsman, bowler) :
	pvp_check = False
	with open('Data/PvP_Probabilities.csv', 'rb') as f:
	    pvp_reader = csv.reader(f)
	    for row in pvp_reader:
	    	if batsman == row[0] and bowler == row[1]:
	    		pvp_check = True
	    		probs_list = row
	    		"""
	    		probs_list 
	    		0,       1,	     2,  3,  4,  5,  6,  7,  8			9
				Batsman, Bowler, 0s, 1s, 2s, 3s, 4s, 6s, Dismissal, BallsFaced
				"""
                break
				
	if pvp_check :		
		probs_list = map(float, probs_list)
		probs_list = probs_list[2:9]
		return pvp_check,probs_list
	else :
		return pvp_check,None
	

def gvg_plist(bat_cluster_number, bowler_cluster_number) :
	
	with open('Data/GvG_Probabilities.csv', 'rb') as f:
	    gvg_reader = csv.reader(f)
	    for row in gvg_reader:
	    	if bat_cluster_number == row[0] and bowler_cluster_number == row[1]:
	    		probs_list = row
	    		"""
	    		probs_list 
	    		0,              1,	           2,  3,  4,  5,  6,  7,  8
				BatsmanCluster, BowlerCluster, 0s, 1s, 2s, 3s, 4s, 6s, Dismissal
				"""
	probs_list = map(float, probs_list)
	probs_list = probs_list[2:]
	return probs_list



def random_pick(some_list, probabilities) :
	
	x = random.uniform(0,sum(probabilities))
	cumulative_probability = 0.0
	for item, item_probability in zip(some_list, probabilities):
		cumulative_probability += item_probability
		if x < cumulative_probability: break
	return item



def innings(bat_order, bow_order, inn) : 
	
	tot_wickets = 0
	m = 1    
	n = 0  

	
	
	bow_index_order = [0,1,0,1,2,3,4,2,3,4,2,3,4,2,3,4,0,1,0,1]  
	x = bow_index_order[0]

	total_runs = 0
	k = -1

	for i in range(0,120) :

		
		if i%6 == 0 :
			k += 1
			x = bow_index_order[k]

			tmp_m = m
			m = n
			n = tmp_m

		curr_bat = bat_order[m] 
		other_bat = bat_order[n] 
		curr_bow = bow_order[x] 
		print 'Batting: ' , curr_bat
		print 'Bowling: ' , curr_bow
		
		existent, pvp_p_list = pvp_plist(curr_bat, curr_bow)  
		if existent :
			prediction = random_pick(discrete_list, pvp_p_list)
		else :
			bat_c_num, bow_c_num = cluster_number(curr_bat, curr_bow)
			gvg_p_list = gvg_plist(bat_c_num, bow_c_num)
			prediction = random_pick(discrete_list, gvg_p_list)
		if prediction== 7 :
			print 'Out'
		else:
			print 'Its a ' , prediction
		
		if prediction==0 or prediction==2 or prediction==4 or prediction==6: 
			total_runs+=prediction 

		
		elif prediction==1 or prediction==3:
			total_runs+=prediction
			tmp_m = m
			m = n
			n = tmp_m
			
		
		else:
			tot_wickets+=1
			m=max(m,n) + 1

			
			if m > 10 :
				break
		print 'Total runs : ' , total_runs
		
		if inn == 2 and total_runs > first_inn_score :
			break
			

	if inn == 1 :
		global first_inn_score
		first_inn_score = total_runs
				
	num_of_overs_played = str(int((i+1)/6)) + "." + str((i+1)%6)  
	return total_runs, str(total_runs)+"/"+str(tot_wickets)+" Overs : "+ num_of_overs_played


# MAIN 
first_innings_score, formatted_score1 = innings(t1_bat_order, t2_bow_order, 1)
print "Team 1 Score : " + formatted_score1

second_innings_score, formatted_score2 = innings(t2_bat_order, t1_bow_order, 2)
print "Team 2 Score : " + formatted_score2

if first_innings_score > second_innings_score :
	print "Team 1 wins!"
elif second_innings_score > first_innings_score :
	print "Team 2 wins!"
else :
	print "Match Tied."

